import React, { Component } from "react";
import { isAuthenticated } from "../auth";
import { Redirect } from "react-router-dom";
import DefaultPost from "../images/mountains.jpg";
import { singlePost, update } from "./apiPost";

class EditPost extends Component {
    constructor() {
        super();
        this.state = {
            id: "",
            title: "",
            body: "",
            redirectToHome: false,
            error: "",
            fileSize: 0,
            loading: false,
        };
    }

    init = (postId) => {
        const token = isAuthenticated().token;
        singlePost(postId, token).then((data) => {
            if (data.error) {
                this.setState({ redirectToHome: true });
            } else {
                this.setState({
                    id: data._id,
                    title: data.title,
                    body: data.body,
                    error: "",
                });
            }
        });
    };

    componentDidMount() {
        this.postData = new FormData();
        const postId = this.props.match.params.postId;
        this.init(postId);
    }

    isValid = () => {
        const { title, body, fileSize } = this.state;
        if (fileSize > 100000) {
            this.setState({
                error: "File size should be less than 100kb",
                loading: false,
            });
            return false;
        }
        if (title.length === 0 || body.length === 0) {
            this.setState({ error: "Name is required", loading: false });
            return false;
        }
        return true;
    };

    handleChange = (name) => (event) => {
        this.setState({ error: "" });
        const value =
            name === "photo" ? event.target.files[0] : event.target.value;

        const fileSize = name === "photo" ? event.target.files[0].size : 0;
        this.postData.set(name, value);
        this.setState({ [name]: value, fileSize });
    };

    clickSubmit = (event) => {
        event.preventDefault();
        this.setState({ loading: true });

        if (this.isValid()) {
            const postId = this.state.id;
            const token = isAuthenticated().token;
            update(postId, token, this.postData).then((data) => {
                if (data.error) this.setState({ error: data.error });
                else
                    this.setState({
                        redirectToHome: true,
                        loading: true,
                        title: "",
                        body: "",
                    });
            });
        }
    };

    postForm = (title, body) => (
        <form>
            <div className="form-group">
                <label className="text-muted">Post Photo</label>
                <input
                    onChange={this.handleChange("photo")}
                    type="file"
                    accept="image/*"
                    className="form-control"
                />
            </div>
            <div className="form-group">
                <label className="text-muted">Title</label>
                <input
                    onChange={this.handleChange("title")}
                    type="text"
                    className="form-control"
                    value={title}
                />
            </div>
            <div className="form-group">
                <label className="text-muted">Body</label>
                <textarea
                    onChange={this.handleChange("body")}
                    type="text"
                    className="form-control"
                    value={body}
                />
            </div>

            <button
                onClick={this.clickSubmit}
                className="btn btn-raised btn-primary"
            >
                Update
            </button>
        </form>
    );

    render() {
        const { id, title, body, redirectToHome, error, loading } = this.state;

        if (redirectToHome) {
            return <Redirect to={`/post/${id}`} />;
        }

        let photoUrl = id
            ? `${process.env.REACT_APP_API_URL}/post/photo/${id}`
            : DefaultPost;

        return (
            <div className="container">
                <h2 className="mt-5 mb-5">{title}</h2>
                <div
                    className="alert alert-danger"
                    style={{ display: error ? "" : "none" }}
                >
                    {error}
                </div>

                {loading ? (
                    <div className="jumbotron text-center">
                        <h2>Loading...</h2>
                    </div>
                ) : (
                    ""
                )}

                <div className="text-center">
                    <img
                        src={photoUrl}
                        alt={title}
                        onError={(i) => (i.target.src = `${DefaultPost}`)}
                        className="img-thunbnail mb-3"
                        style={{
                            height: "250px",
                            width: "100%",
                            objectFit: "cover",
                        }}
                    />
                </div>

                {this.postForm(title, body)}
            </div>
        );
    }
}

export default EditPost;
