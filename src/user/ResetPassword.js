import React, { Component } from "react";
import { resetPassword } from "../auth";

class ResetPassword extends Component {
    constructor(props) {
        super();
        this.state = {
            newPassword: "",
            message: "",
            error: "",
            loading: false,
        };
    }

    resetPassword = (e) => {
        e.preventDefault();
        this.setState({ message: "", error: "", loading: true });

        resetPassword({
            newPassword: this.state.newPassword,
            resetPasswordLink: this.props.match.params.resetPasswordToken,
        }).then((data) => {
            if (data.error) {
                console.log(data.error);
                this.setState({ error: data.error, loading: false});
            } else {
                this.setState({
                    message: data.message,
                    newPassword: "",
                    loading: false,
                });
            }
        });
    };

    render() {
        return (
            <div className="container">
                <h2 className="mt-5 mb-5">Reset Password</h2>

                <div
                    className="alert alert-danger"
                    style={{ display: this.state.error ? "" : "none" }}
                >
                    {this.state.error}
                </div>
                
                <div
                    className="alert alert-info"
                    style={{ display: this.state.message ? "" : "none" }}
                >
                    {this.state.message}
                </div>

                {this.state.loading ? (
                    <div className="jumbotron text-center">
                        <h2>Loading...</h2>
                    </div>
                ) : (
                    ""
                )}

                <form>
                    <div className="form-group">
                        <label className="text-muted">New Password</label>
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Your new password"
                            value={this.state.newPassword}
                            name="newPassword"
                            onChange={(e) =>
                                this.setState({
                                    newPassword: e.target.value,
                                    message: "",
                                    error: "",
                                })
                            }
                            autoFocus
                        />
                    </div>
                    <button
                        onClick={this.resetPassword}
                        className="btn btn-raised btn-primary"
                    >
                        Reset Password
                    </button>
                </form>
            </div>
        );
    }
}

export default ResetPassword;
