import React, { Component } from "react";
import { forgotPassword } from "../auth";

class ForgotPassword extends Component {
    state = {
        email: "",
        error: "",
        message: "",
        loading: false,
    };

    forgotPassword = (e) => {
        e.preventDefault();
        this.setState({ message: "", error: "", loading: true });
        forgotPassword(this.state.email).then((data) => {
            if (data.error) {
                this.setState({ error: data.error });
            } else {
                this.setState({ message: data.message, loading: false });
            }
        });
    };

    forgotPassowrdForm = (email) => (
        <form>
            <div className="form-group">
                <label className="text-muted">Email</label>
                <input
                    onChange={(e) => {
                        this.setState({
                            email: e.target.value,
                            message: "",
                            error: "",
                        });
                    }}
                    placeholder="Your email address"
                    type="email"
                    className="form-control"
                    value={email}
                />
            </div>

            <button
                onClick={this.forgotPassword}
                className="btn btn-raised btn-primary"
            >
                Send Password Rest Link
            </button>
        </form>
    );

    render() {
        const { email, error, message } = this.state;

        return (
            <div className="container">
                <h2 className="mt-5 mb-5">Forgot Password</h2>

                <div
                    className="alert alert-danger"
                    style={{ display: error ? "" : "none" }}
                >
                    {error}
                </div>
                
                <div
                    className="alert alert-info"
                    style={{ display: message ? "" : "none" }}
                >
                    {message}
                </div>

                {/* {loading ? (
                    <div className="jumbotron text-center">
                        <h2>Loading...</h2>
                    </div>
                ) : (
                    ""
                )} */}

                {this.forgotPassowrdForm(email)}
            </div>
        );
    }
}

export default ForgotPassword;
